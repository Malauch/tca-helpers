// swift-tools-version: 5.7
import PackageDescription

extension ExternalDependency {
	static let composableArchitecture = Self(
		url: "https://github.com/pointfreeco/swift-composable-architecture",
		from: "1.0.0", 
		name: "ComposableArchitecture",
		package: "swift-composable-architecture"
	)
	
	static let helpers = Self(
		url: "https://gitlab.com/Malauch/helpers",
		branch: "main",
		name: "Helpers",
		package: "helpers"
	)
}

extension Module {
	static let tcaHelpers = Self(
		name: "TCAHelpers",
		dependencies: [
			.external(.composableArchitecture),
			.external(.helpers),
		],
		testTarget: TestTarget()
	)
}

// MARK: - Package definition
let package = Package(
	name: "TCAHelpers",
	platforms: [.iOS(.v15), .watchOS(.v8), .macOS(.v12), .tvOS(.v15), .macCatalyst(.v15)]
)

package.setUpDependencies([
	.composableArchitecture,
	.helpers
])

package.setUpModules([
	.tcaHelpers
])

// MARK: - Helpers
fileprivate struct ExternalDependency {
	let packageDependency: Package.Dependency
	let targetDependency: Target.Dependency
	
	init(dependency: Package.Dependency, target: Target.Dependency) {
		self.packageDependency = dependency
		self.targetDependency = target
	}
	
	init(
		url: String,
		from version: String,
		name: String,
		moduleAliases: [String: String]? = nil,
		package: String
	) {
		self.packageDependency = .package(url: url, from: .init(stringLiteral: version))
		self.targetDependency = .product(name: name, package: package, moduleAliases: moduleAliases)
	}
	
	init(
		url: String,
		branch: String,
		name: String,
		moduleAliases: [String: String]? = nil,
		package: String
	) {
		self.packageDependency = .package(url: url, branch: branch)
		self.targetDependency = .product(name: name, package: package, moduleAliases: moduleAliases)
	}
	
	init(
		url: String,
		revision: String,
		name: String,
		moduleAliases: [String: String]? = nil,
		package: String
	) {
		self.packageDependency = .package(url: url, revision: revision)
		self.targetDependency = .product(name: name, package: package, moduleAliases: moduleAliases)
	}
}

extension Module {
	fileprivate enum Dependency {
		case `internal`(Module)
		case external(ExternalDependency)
		case additional(Target.Dependency)
	}
}

// NB: Make it simple for now. One Library, one target with the same name etc. Refactor when more option needed.
fileprivate struct Module {
	private let dependencies: [Dependency]
	private let _testTarget: TestTarget?
	private let name: String
	private let excludes: [String]
	private let resources: [Resource]?
	
	fileprivate init(
		name: String,
		dependencies: [Dependency] = [],
		testTarget: TestTarget? = nil,
		excludes: [String] = [],
		resources: [Resource]? = nil
	) {
		self.name = name
		self.dependencies = dependencies
		self._testTarget = testTarget
		self.excludes = excludes
		self.resources = resources
	}
	
	var product: Product {
		.library(name: name, targets: [name])
	}
	
	var target: Target {
		let targetDependencies: [Target.Dependency] = dependencies.reduce(into: []) { partialResult, dependency in
			switch dependency {
			case let .internal(value):
				partialResult.append(.init(stringLiteral: value.name))
			case let .external(value):
				partialResult.append(value.targetDependency)
			case let .additional(value):
				partialResult.append(value)
			}
		}
		
		return .target(
			name: name,
			dependencies: targetDependencies,
			exclude: excludes,
			resources: resources
		)
	}
	
	var testTarget: Target? {
		guard let _testTarget else { return nil  }
		
		return .testTarget(name: _testTarget.name ?? name + "Tests", dependencies: [.byName(name: name)], resources: _testTarget.resources)
	}
}

fileprivate struct TestTarget {
	let name: String?
	let resources: [Resource]?
	
	init(
		name: String? = nil,
		resources: [Resource]? = nil
	) {
		self.name = name
		self.resources = resources
	}
}

extension Package {
	fileprivate func setUpDependencies(_ dependencies: [ExternalDependency]) {
		self.dependencies = dependencies.map(\.packageDependency)
	}
	
	fileprivate func setUpModules(_ modules: [Module]) {
		self.products = modules.map(\.product)
		self.targets = modules.map(\.target)
		self.targets.append(contentsOf: modules.compactMap(\.testTarget))
	}
}
