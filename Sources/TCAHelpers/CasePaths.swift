import CasePaths

extension CasePath {
	public func isMatching(_ value: Root) -> Bool {
		self ~= value
	}
	
	public func isNotMatching(_ value: Root) -> Bool {
		!(self ~= value)
	}
}
