//import ComposableArchitecture
//
//public protocol EffectBuilder {
//	associatedtype Output
//	associatedtype Failure: Error
//	
//	static func buildOptional(_ component: _EffectPublisher<Output>?) -> _EffectPublisher<Output>
//	
//	static func buildEither(first component: _EffectPublisher<Output>) -> _EffectPublisher<Output>
//	
//	static func buildEither(second component: _EffectPublisher<Output>) -> _EffectPublisher<Output>
//	
//	static func buildExpression(_ expression: _EffectPublisher<Output>) -> _EffectPublisher<Output>
//}
//
//extension EffectBuilder {
//	public static func buildOptional(_ component: _EffectPublisher<Output>?) -> _EffectPublisher<Output> {
//		return component ?? .none
//	}
//	
//	public static func buildEither(first component: _EffectPublisher<Output>) -> _EffectPublisher<Output> {
//		component
//	}
//	
//	public static func buildEither(second component: _EffectPublisher<Output>) -> _EffectPublisher<Output> {
//		component
//	}
//	
//	public static func buildExpression(_ expression: _EffectPublisher<Output>) -> _EffectPublisher<Output> {
//		expression
//	}
//}
//
//@resultBuilder
//public struct ConcatenateBuilder<Output, Failure> where Failure: Error {
//	public static func buildBlock(_ components: _EffectPublisher<Output>...) -> _EffectPublisher<Output> {
//		.concatenate(components)
//	}
//	
//	public static func buildArray(_ components: [_EffectPublisher<Output>]) -> _EffectPublisher<Output> {
//		.concatenate(components)
//	}
//}
//
//extension ConcatenateBuilder: EffectBuilder { }
//
//@resultBuilder
//public struct MergeBuilder<Output, Failure> where Failure: Error {
//	public static func buildBlock(_ components: _EffectPublisher<Output>...) -> _EffectPublisher<Output> {
//		.merge
//	}
//	
//	public static func buildArray(_ components: [_EffectPublisher<Output>]) -> EffectPublisher<Output, Failure> {
//		.merge(components)
//	}
//}
//
//extension MergeBuilder: EffectBuilder { }
//
//extension _EffectPublisher {
//	/// Concatenate EffectPublisher with result builder closure
//	public static func concatenate(@ConcatenateBuilder<Output, Failure> _ effects: () -> EffectPublisher<Action, Failure>) -> EffectPublisher<Action, Failure> {
//		effects()
//	}
//	
//	/// Merge EffectPublisher with result builder closure
//	public static func merge(@MergeBuilder<Output, Failure> _ effects: () -> EffectPublisher<Action, Failure>) -> EffectPublisher<Action, Failure> {
//		effects()
//	}
//}
