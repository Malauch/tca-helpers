//import ComposableArchitecture
//import Combine
//import Foundation
//
//extension Effect {
//	fileprivate struct RepeatWhileOperatorTimerId: Hashable {}
//	fileprivate struct RepeatUntilOperatorTimerId: Hashable {}
//	
//	/// Repeats effect on provided interval with max repeats and repeat condition options.
//	///
//	/// Use when you would like repeat work of some "single fire" side effect.
//	/// It's like `.retry` but on specific value not only failure.
//	/// - Parameters:
//	/// 	- interval: Time interval between repeats.
//	/// 	- tolerance: Tolerance of the intervals (it's timer under the hood).
//	/// 	- scheduler: Scheduler.
//	/// 	- options: Scheduler options.
//	///		- tries: Max number of repeats. When it's equal to `0` then it return just original effect.
//	///		- while: Predicate which has to be true to fire repeat. First `false` value cancel repeating.
//	/// - Returns: Effect
//	@available(*, deprecated, message: "Use async/await effects instead")
//	public func repeatWhile<S>(
//		interval: S.SchedulerTimeType.Stride,
//		tolerance: S.SchedulerTimeType.Stride? = nil,
//		on scheduler: S,
//		options: S.SchedulerOptions? = nil,
//		max tries: Int,
//		while predicate: @escaping (Output) -> Bool
//	) -> EffectPublisher<Action, Failure> where S: Scheduler {
//		guard tries > 0
//		else { return self }
//		
//		return EffectTask<S.SchedulerTimeType>
//			.timer(id: RepeatWhileOperatorTimerId(), every: interval, on: scheduler)
//			.map { _ in self }
//			.switchToLatest()
//			.prefix(tries - 1)
//			.prefix(while: predicate)
//			.prepend(self)
//			.eraseToEffect()
//	}
//	
//	/// Repeats effect on provided interval with max repeats and repeat condition options.
//	///
//	/// Use when you would like repeat work of some "single fire" side effect.
//	/// It's like `.retry` but on specific value not only failure.
//	/// - Parameters:
//	/// 	- interval: Time interval between repeats.
//	/// 	- tolerance: Tolerance of the intervals (it's timer under the hood).
//	/// 	- scheduler: Scheduler.
//	/// 	- options: Scheduler options.
//	///		- tries: Max number of repeats. When it's equal to `0` then it return just original effect.
//	///		- until: Predicate which has to be true to cancel repeating. First value which returned `true` value is published.
//	/// - Returns: Effect
//	@available(*, deprecated, message: "Use async/await effects instead")
//	public func repeatUntil<S>(
//		interval: S.SchedulerTimeType.Stride,
//		tolerance: S.SchedulerTimeType.Stride? = nil,
//		on scheduler: S,
//		options: S.SchedulerOptions? = nil,
//		max tries: Int,
//		until predicate: @escaping (Output) -> Bool
//	) -> EffectPublisher<Action, Failure> where S: Scheduler {
//		guard tries > 0
//		else { return self }
//		
//		let stopTrigger = PassthroughSubject<Output, Failure>()
//		
//		return EffectTask<S.SchedulerTimeType>
//			.timer(id: RepeatUntilOperatorTimerId(), every: interval, on: scheduler)
//			.map { _ in self }
//			.prepend(self)
//			.switchToLatest()
//			.prefix(untilOutputFrom: stopTrigger)
//			.handleEvents(receiveOutput: { value in
//				if predicate(value) {
//					stopTrigger.send(value)
//					stopTrigger.send(completion: .finished)
//				}
//			})
//			.merge(with: stopTrigger)
//			.prefix(tries)
//			.eraseToEffect()
//	}
//}

//extension Effect where Failure == Never {
//	/// Convenience method to wrap AsyncStream to Effect and publish its element as Effect output.
//	///
//	/// Instead of:
//	/// ```
//	/// .run { send in
//	///	  for await item in stream {
//	///     send(item)
//	///	  }
//	/// }
//	/// ```
//	/// you can use:
//	/// ```
//	/// .run(asyncStream: stream)
//	/// ```
//	/// or if you need to do something with AsyncStream element:
//	/// ```
//	/// .run(asyncStream: stream) { doSomething($0) }
//	/// ```
//	@available(*, deprecated, message: "Temporary deprecated. It has to be tested, and fixed or deleted to use with TCA 1.0")
//	public static func run<Element>(
//		priority: TaskPriority? = nil,
//		asyncStream: AsyncStream<Element>,
//		operation: @escaping @Sendable (Element) async throws -> Output = { (element: Element) in return element },
//		catch handler: (@Sendable (Error, Send<Self.Output>) async -> Void)? = nil,
//		file: StaticString = #file,
//		fileID: StaticString = #fileID,
//		line: UInt = #line
//	) -> Self {
//		Self.run(
//			priority: priority,
//			operation: { send in
//				for try await item in asyncStream {
//					try await send(operation(item))
//				}
//			},
//			catch: handler,
//			file: file,
//			fileID: fileID,
//			line: line
//		)
//	}
//}

//extension Effect where Failure == Never {
//	/// Convenience method to wrap AsyncStream to Effect and publish its element as Effect output.
//	///
//	/// Instead of:
//	/// ```
//	/// .run { send in
//	///	  for await item in stream {
//	///     send(item)
//	///	  }
//	/// }
//	/// ```
//	/// you can use:
//	/// ```
//	/// .run(asyncStream: stream) {
//	///   item
//	/// }
//	/// ```
//	@available(*, deprecated, message: "Temporary deprecated. It has to be tested, and fixed or deleted to use with TCA 1.0")
//	public static func run<Element>(
//		priority: TaskPriority? = nil,
//		asyncStream: AsyncThrowingStream<Element, any Error>,
//		operation: @escaping @Sendable (Element) async throws -> Output,
//		catch handler: (@Sendable (Error, Send<Self.Output>) async -> Void)? = nil,
//		file: StaticString = #file,
//		fileID: StaticString = #fileID,
//		line: UInt = #line
//	) -> Self {
//		Self.run(
//			priority: priority,
//			operation: { send in
//				for try await item in asyncStream {
//					try await send(operation(item))
//				}
//			},
//			catch: handler,
//			file: file,
//			fileID: fileID,
//			line: line
//		)
//	}
//}
