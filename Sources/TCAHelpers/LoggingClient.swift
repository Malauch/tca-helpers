import ComposableArchitecture
import Foundation
import Helpers
import os
import XCTestDynamicOverlay

/// Client for interacting with ``os.Logger`` in controllable/testable way.
///
/// Interface is a little non-standard/weird for dependency client for TCA. You have to
/// call logger in reducer via callback. It's necessary, because ``os.Logger`` has some
/// unusual interface, which accepts only string interpolation as parameter. Therefore,
///  it's not possible to create standard wrapper type with endpoints which pass parameters
///  to original Logger.
public struct LoggingClient {
	/// Endpoint for saving logs
	/// - Parameter callback: callback which exposes Logger.
	private var log: @Sendable (@escaping (Logger) -> Void) -> Void
	
	/// Private initializer for defining ``LoggingClient`` with injected additional logic
	/// for tests and debug purposes.
	/// - Parameter log: callback which exposes Logger
	private init(log: @Sendable @escaping (@escaping (Logger) -> Void) -> Void) {
		self.log = log
	}
	
	/// Initialize ``LoggerClient`` with selected Logger.
	/// - Parameter logger: Logger used for logging purposes
	private init(_ logger: Logger) {
		self.log = { callback in callback(logger)}
	}
	
	
	/// Convenience method which make possible to call LoggingClient instance as function and directly access ``log`` endpoint.
	/// - Parameter callback: Callback which exposes Logger
	@available(*, deprecated, message: "Use LoggerGenerator instead")
	public func callAsFunction(_ callback: @escaping (Logger) -> Void) -> Void {
		log(callback)
	}
}

extension LoggingClient {
	/// Live implementation of `LoggingClient`
	@available(*, deprecated, message: "Use LoggerGenerator instead")
	public static func live(logger: Logger) -> Self {
		Self(logger)
	}
	
	/// Live implementation of `LoggingClient`. Subsystem is current main bundle identifier.
	@available(*, deprecated, message: "Use LoggerGenerator instead")
	public static func live(category: String) -> Self {
		Self(.forMainBundle(category: category))
	}
}

#if DEBUG
extension LoggingClient {
	
	/// Initialize `LoggingClient` with **disabled** Logger and instead of logging, calls
	/// passed operation.
	/// - Parameter operation: Operation which is called on very logging event.
	/// - Returns: `LoggingClient`
	///
	/// It's used strictly for testing logging.
	/// ```
	///	var count = 0
	///
	///	let logging = LoggingClient.test { count += 1 }  // increases count on every log
	///	logging { $0.error("Some error") }  // write some error log
	///
	///	count == 1  // true
	/// ```
	@available(*, deprecated, message: "Use LoggerGenerator instead")
	public static func test(_ operation: @escaping () -> Void) -> Self {
		Self { callback in
			operation()
			callback(Logger(.disabled))
		}
	}
	
	@available(*, deprecated, message: "Use LoggerGenerator instead")
	public static var noop = Self(Logger(.disabled))
	
	@available(*, deprecated, message: "Use LoggerGenerator instead")
	public static var unimplemented = Self(
		log: XCTUnimplemented("\(Self.self).log", placeholder: ())
	)
}
#endif

public struct LoggerGenerator: Sendable {
	private let generate: @Sendable () -> Logger
	
	public func callAsFunction() -> Logger {
		self.generate()
	}
	
	public static func live(_ category: String) -> Self {
		Self { Logger.forMainBundle(category: category) }
	}
	
	public static func noop() -> Self {
		Self { Logger(.disabled) }
	}
	
	public static func unimplemented(_ name: String) -> Self {
		Self(generate:
					XCTUnimplemented("Unimplemented: @Dependency(\\.\(name))",
													 placeholder: Logger(.disabled))
		)
	}
}

private enum LoggerGeneralKey: DependencyKey {
	static let liveValue = LoggerGenerator.live("General Logger")
	static let testValue = LoggerGenerator.unimplemented("General logger")
	static let previewValue = LoggerGenerator.noop()
}

extension DependencyValues {
	public var loggerGeneral: LoggerGenerator {
		get { self[LoggerGeneralKey.self] }
		set { self[LoggerGeneralKey.self] = newValue }
	}
}
