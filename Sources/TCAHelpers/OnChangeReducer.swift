import ComposableArchitecture

extension Reducer {
	/// Function which transform `ReducerProtocol` instance to `OnChangeReducer` which observes selected
	/// state and inject new action when this state changed.
	/// - Parameters:
	///   - toMonitoredState: Function from `State` to `MonitoredState`. Most of the time in form of KeyPath.
	///   - additionalEffect: Function which return additional effects when observed state changes. This function is
	///   called with both, previous and current values of observed state.
	///
	///   	additionalEffect parameters (in order as below):
	///   	- old value of observed state
	///   	- new value of observed state
	///   	- current inout state
	///   	- action which changed observed value
	///
	/// - Returns: `EffectTask<Action>`
	@available(*, deprecated, message: "Use built-in version from The Compsable Architecture")
	@inlinable
	public func onChange<ObservedState: Equatable>(
		of toObservedState: @escaping (State) -> ObservedState,
		perform additionalEffects: @escaping (ObservedState, ObservedState, inout State, Action) -> Effect<Action>
	) -> OnChangeReducer<Self, ObservedState> {
		OnChangeReducer(parent: self, toObservedState: toObservedState, additionalEffects: additionalEffects)
	}
	
	/// Function which transform `ReducerProtocol` instance to `OnChangeReducer` which observes selected
	/// state and inject new action when this state changed.
	/// - Parameters:
	///   - toMonitoredState: Function from `State` to `MonitoredState`. Most of the time in form of KeyPath.
	///   - additionalEffect: Function which return additional effects when observed state changes. This function is
	///   called with both, previous and current values of observed state.
	///
	///   	additionalEffect parameters (in order as below):
	///   	- new value of observed state
	///   	- current inout state
	///   	- action which changed observed value
	///
	/// - Returns: `EffectTask<Action>`
	@available(*, deprecated, message: "Use built-in version from The Compsable Architecture")
	@inlinable
	public func onChange<ObservedState: Equatable>(
		of toObservedState: @escaping (State) -> ObservedState,
		perform additionalEffects: @escaping (ObservedState, inout State, Action) -> Effect<Action>
	) -> OnChangeReducer<Self, ObservedState> {
		OnChangeReducer(
			parent: self,
			toObservedState: toObservedState,
			additionalEffects: { _, newObservedState, state, action in
				additionalEffects(newObservedState, &state, action)
			}
		)
	}
}

/// Higher order reducer which can inject state mutation or effect on change of state's property.
public struct OnChangeReducer<Parent: Reducer, ObservedState: Equatable>: Reducer {
	/// Parent reducer, which is transformed to `OnChangeReducer`
	@usableFromInline
	let parent: Parent
	/// Function which takes `Parent.State` and returns monitored state. For most of a time it is in form of `KeyPath<Parent.State, ObservedState>`
	@usableFromInline
	let toObservedState: (Parent.State) -> ObservedState
	/// Function which returns effects when monitored state is changed.
	/// - Parameters:
	///   - previousValue: Previous value of monitored state
	///   - currentValue: Current (new) value of monitored state
	///   - state: current state (inout)
	///   - action: current action
	@usableFromInline
	let additionalEffects: (ObservedState, ObservedState, inout Parent.State, Parent.Action) -> Effect<Parent.Action>
	
	@usableFromInline
	init(
		parent: Parent,
		toObservedState: @escaping (Parent.State) -> ObservedState,
		additionalEffects: @escaping (ObservedState, ObservedState, inout Parent.State, Parent.Action) -> Effect<Parent.Action>) {
		self.parent = parent
		self.toObservedState = toObservedState
		self.additionalEffects = additionalEffects
	}
	
	@inlinable
	public func reduce(into state: inout Parent.State, action: Parent.Action) -> Effect<Parent.Action> {
		let previousObservedState = toObservedState(state)
		let effects = self.parent.reduce(into: &state, action: action)
		let newObservedState = toObservedState(state)
		
		return previousObservedState != newObservedState
		? .merge(effects, additionalEffects(previousObservedState, newObservedState, &state, action))
		: effects
	}
}
