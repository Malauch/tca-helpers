import ComposableArchitecture

/// Scoping Reducer to Void state.
///
/// It's used when composing parent reducer with stateless child reducer.
///
/// **Disclaimer:** It's weird that there is not built-in operator in TCA. Maybe it
/// is wrong approach?
public struct ScopeToVoid<ParentState, ParentAction, Child: Reducer>: Reducer
where Child.State == Void
{
	let child: Child
	let toChildAction: CasePath<ParentAction, Child.Action>
	
	public init(
		action toChildAction: CasePath<ParentAction, Child.Action>,
		@ReducerBuilder<Child.State, Child.Action> _ child: () -> Child
	) {
		self.toChildAction = toChildAction
		self.child = child()
	}
	
	public func reduce(into state: inout ParentState, action: ParentAction) -> Effect<ParentAction> {
		var stateToDiscard: Void = ()
		guard let childAction = toChildAction.extract(from: action)
		else { return .none }
		
		return self.child
			.reduce(into: &stateToDiscard, action: childAction)
			.map(toChildAction.embed)
	}
}
