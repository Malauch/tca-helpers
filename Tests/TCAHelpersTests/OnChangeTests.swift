import Combine
import ComposableArchitecture
import Helpers
@testable import TCAHelpers
import XCTest

@MainActor
final class OnChangeTests: XCTestCase {
	private struct Counter: Reducer {
		public struct State: Equatable {
			var count = 0
			var message: String? = nil
		}
		
		public enum Action: Equatable {
			case increase
			case addMessage(String?)
		}
		
		public var body: some Reducer<State, Action> {
			Reduce { state, action in
				switch action {
				case .increase:
					state.count += 1
					return .none
				case let .addMessage(message):
					state.message = message
					return .none
				}
			}
		}
	}
	
	override func setUp() {
	}
	
	func testOnChangeOfValue() async {
		let store = TestStore(
			initialState: .init(),
			reducer: Counter()
				.onChange(of: \.count) { count, state, action in
					if count % 3 == 0 {
						return .task { .addMessage("Count: \(count)") }
					}
					return .none
				}
		)
		
		XCTAssertEqual(store.state, .init(count: 0, message: nil))
		_ = await store.send(.increase) {
			$0.count = 1
		}
		_ = await store.send(.increase) {
			$0.count = 2
		}
		_ = await store.send(.increase) {
			$0.count = 3
		}
		await store.receive(.addMessage("Count: 3")) {
			$0.message = "Count: 3"
		}
	}
	
	func testOnChangeCompareNewAndOldValue() async {
		let store = TestStore(
			initialState: .init(),
			reducer: Counter()
				.onChange(of: \.count) { previousCount, count, state, action in
					if count % 3 == 0 {
						return .task { .addMessage("Count: \(count)") }
					}
					if previousCount % 3 == 0 && previousCount != 0 {
						return .task { .addMessage(nil) }
					}
					return .none
				}
		)
		
		XCTAssertEqual(store.state, .init(count: 0, message: nil))
		_ = await store.send(.increase) {
			$0.count = 1
		}
		_ = await store.send(.increase) {
			$0.count = 2
		}
		_ = await store.send(.increase) {
			$0.count = 3
		}
		await store.receive(.addMessage("Count: 3")) {
			$0.message = "Count: 3"
		}
		_ = await store.send(.increase) {
			$0.count = 4
		}
		await store.receive(.addMessage(nil)) {
			$0.message = nil
		}
	}
}
