import Combine
import ComposableArchitecture
import Helpers
import XCTest

final class TCAHelperTests: XCTestCase {
	static var globalCounter = 0
	let effect = Effect<Int>.result {
		globalCounter += 1
		return .success(globalCounter)
	}
	
	struct State: Equatable {
		var counter = 0
	}
	
	enum Action: Equatable {
		case fireEffect
		case update(Int)
	}
	
	struct Environment {
		var effect: (AnySchedulerOf<DispatchQueue>) -> Effect<Int>
		var queue: AnySchedulerOf<DispatchQueue>
	}
	
	let reducer = AnyReducer<State, Action, Environment> { state, action, environment in
		switch action {
		case .fireEffect:
			return environment.effect(environment.queue)
				.map(Action.update)
				.eraseToEffect()
		case .update(let value):
			state.counter = value
			return .none
		}
	}
	
	
	override func setUp() {
		Self.globalCounter = 0
	}
	
	// MARK: - While: Stop with predicate
	func testRepeatWhileEffect_stopWithPredicate() {
		
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatWhile(interval: 1, on: queue, max: 99) { counter in
							counter < 5
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(2)) {
			$0.counter = 2
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(3)) {
			$0.counter = 3
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(4)) {
			$0.counter = 4
		}
		
		queue.advance(by: 1)  // One more timer tick is needed to cancel it.
	}
	
	// MARK: - While: Stop repeating with max repeats arguments
	func testRepeatWhileEffect_stopMaxRepeats() {
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatWhile(interval: 1, on: queue, max: 3) { _ in
							true
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(2)) {
			$0.counter = 2
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(3)) {
			$0.counter = 3
		}
		
		queue.advance(by: 1)
	}
	
	// MARK: - While: Single run
	func testRepeatWhileEffect_singleRun() {
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatWhile(interval: 1, on: queue, max: 99) { _ in
							false
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 1)
	}
	
	// MARK: - While: Max tries == 1
	func testRepeatEffect_singleRunMaxTries1() {
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatWhile(interval: 1, on: queue, max: 0) { _ in
							true
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 1)
	}
	
	// MARK: - Until: Stop with predicate
	func testRepeatUntilEffect_stopWithPredicate() {
		
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatUntil(interval: 1, on: queue, max: 99) { counter in
							counter == 3
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(2)) {
			$0.counter = 2
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(3)) {
			$0.counter = 3
		}
		
		queue.advance(by: 5)
	}
	
	// MARK: - Until: Stop repeating with max repeats arguments
	func testRepeatUntilEffect_stopMaxRepeats() {
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatUntil(interval: 1, on: queue, max: 3) { _ in
							false
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(2)) {
			$0.counter = 2
		}
		
		queue.advance(by: 1)
		testStore.receive(.update(3)) {
			$0.counter = 3
		}
		
		queue.advance(by: 5)
	}
	
	// MARK: - Until: Single run
	func testRepeatUntilEffect_singleRun() {
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatUntil(interval: 1, on: queue, max: 99) { _ in
							true
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 5)
	}
	
	// MARK: - Until: Max tries == 1
	func testRepeatUntilEffect_singleRunMaxTries1() {
		let queue = DispatchQueue.test
		let testStore = TestStore(
			initialState: State(),
			reducer: reducer,
			environment: .init(
				effect: { queue in
					self.effect
						.repeatUntil(interval: 1, on: queue, max: 0) { _ in
							true
						}
				},
				queue: queue.eraseToAnyScheduler()
			)
		)
		
		testStore.send(.fireEffect)
		testStore.receive(.update(1)) {
			$0.counter = 1
		}
		
		queue.advance(by: 5)
	}
}
