import Combine
import ComposableArchitecture
import Helpers
@testable import TCAHelpers
import XCTest

@MainActor
final class ScopeToVoidTests: XCTestCase {
	
	struct CounterComposed: Reducer {
		var someClient: @Sendable () async -> Void
		struct State: Equatable {
			var count = 0
		}
		
		enum Action: Equatable {
			case increase
			case stateless(Stateless.Action)
		}
		
		var body: some Reducer<State, Action> {
			ScopeToVoid(action: /Action.stateless) {
				Stateless(someClient: someClient)
			}
			
			Reduce { state, action in
				switch action {
				case .increase:
					state.count += 1
					return .none
				case .stateless:
					return .none
				}
			}
		}
	}
	
	public struct Stateless: Reducer {
		var someClient: @Sendable () async -> Void
		typealias State = Void
		
		public enum Action: Equatable {
			case operation
		}
		
		public func reduce(into state: inout State, action: Action) -> Effect<Action> {
			switch action {
			case .operation:
				return .fireAndForget {
					await self.someClient()
				}
			}
		}
	}
	
	
	
	func testBasics() async {
		let clientState = ActorIsolated(IOCounter<Blank>())
		
		
		let store = TestStore(
			initialState: .init(),
			reducer: CounterComposed(
				someClient: { await clientState.withValue { $0.write() } }
			)
		)
		
		_ = await store.send(.increase) {
			$0.count = 1
		}
		var writeCount = await clientState.writeCount
		XCTAssertEqual(writeCount, 0)
		
		_ = await store.send(.stateless(.operation))
		await store.finish()
		writeCount = await clientState.writeCount
		XCTAssertEqual(writeCount, 1)
	}
	
}
